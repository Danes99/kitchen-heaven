// Require pre-installed modules
const path = require('path')

// Require downloaded modules
const express = require('express')

// Require routers
searchRouter = require('./routers/search')
otherRouter = require('./routers/other')
recipeRouter = require('./routers/recipe')
userRouter = require('./routers/user')

const port = process.env.PORT

// Define paths for Express config
const viewsPath = path.join(__dirname, '../templates/views')

// Connect to the database (MongoDB) using mongoose
require('./db/mongoose')

// Create app
const app = express()

// Define express config
app.use(express.json())
app.set('view engine', 'hbs')
app.set('views', viewsPath)

// Use routers
app.use(searchRouter)
app.use(recipeRouter)
app.use(otherRouter)
app.use(userRouter)

// Start server
app.listen(
    port,
    () => console.log('Server is up on port', port)
)