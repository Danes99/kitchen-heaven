// This module take in input a list of object
// Input: list of recipes found by the database
// Return html code as a string
// To be inserted dynamicly in the html page

recipeObjectToPartials = recipeObject =>
(
    `<div 
        id=${recipeObject._id} 
        class="search-result"
        onclick="window.location='/recipes/${recipeObject._id}';"
    >
        <p>
            ${recipeObject.name}<br/>
            ${recipeObject.country}, ${recipeObject.language}
        </p>
    </div>`
)

// recipeObjectToPartials = recipeObject =>
// (
//     `<div 
//         id=${recipeObject._id} 
//         class="descriptif_recette"
//         onclick="window.location='/recipes/${recipeObject._id}';"
//     >
//         <p>
//             <strong>
//                 ${recipeObject.name}<br/>
//                 ${recipeObject.country}, ${recipeObject.language}
//             </strong>
//     </div>`
// )

searchResultToPartials = recipeArray =>
(
    recipeArray.map( recipe => recipeObjectToPartials(recipe) )
    .join('')
)

module.exports = searchResultToPartials