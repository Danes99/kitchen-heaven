// Require pre-installed modules
const path = require('path')

// Require downloaded modules
const express = require('express')
const hbs = require('hbs')

// Define paths for Express config
const publicDirectoryPath = path.join(__dirname, '../../public')
const partialsPath = path.join(__dirname, '../../templates/partials')

const router = new express.Router()

// router.set('view engine', 'hbs')
// router.set('views', viewsPath)
hbs.registerPartials(partialsPath);
router.use(express.static(publicDirectoryPath))

// Home page - Search page
router.get(
    '',
    (req ,res) => 
    {
        res.render(
            'elhadji/page_accueil',
            {
                title: 'Kitchen Heaven'
            }
        ) 
    }
)


// Aboutn us page
router.get(
    '/about',
    (req ,res) => 
    {
        res.render(
            'elhadji/page_about',
            {
                title: 'About us'
            }
        ) 
    }
)

// Sign in page
router.get(
    '/signin',
    (req ,res) => 
    {
        res.render(
            'elhadji/page_connexion',
            {
                title: 'Sign in'
            }
        ) 
    }
)


// Sign up page
router.get(
    '/signup',
    (req ,res) => 
    {
        res.render(
            'elhadji/page_inscription',
            {
                title: 'Sign up'
            }
        ) 
    }
)


// Help page
router.get(
    '/help',
    (req ,res) => 
    {
        res.render(
            'elhadji/page_aide',
            {
                title: 'Help'
            }
        ) 
    }
)

// 404 not found page
router.get(
    '*', 
    (req, res) =>
    {
        res.render(
            '404',
            {
                title: 'Page not found',
                textDisplay: 'Error 404. Page not found.'
            }
        )
    }
)

module.exports = router