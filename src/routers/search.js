// Require pre-installed modules
const path = require('path')

// Require downloaded modules
const express = require('express')
const hbs = require('hbs')

// Require MongoDB models
const Recipe = require('../models/recipe')

// Require functions
searchResultsToPartials = require('../utils/searchResultsToPartials')

// Define paths for Express config
const publicDirectoryPath = path.join(__dirname, '../../public')
const partialsPath = path.join(__dirname, '../../templates/partials')

const router = new express.Router()

// router.set('view engine', 'hbs')
// router.set('views', viewsPath)
hbs.registerPartials(partialsPath);
router.use(express.static(publicDirectoryPath))

router.get(
    '/search',
    async (req, res) => 
    {
        // Search parameters
        const recipeName = req.query.recipe
        const country = req.query.country
        const language = req.query.language

        try 
        {
            const recipe = await Recipe.find({ recipeName }).limit(10)

            if (!recipe)
            {
                return res.status(404).send()
            }
            
            // res.status(201).send(recipe)

            res.render(
                'elhadji/page_results.hbs',
                {
                    title: 'Search results',
                    searchResults: searchResultsToPartials(recipe)
                }
            ) 
            
        } 
        catch (error) 
        {
            res.status(500).send(error)
        }

        // res.render(
        //     'search_result',
        //     {
        //         title: 'Search results',
        //         recipeName,
        //         country,
        //         language,
        //         username
        //     }
        // ) 
    }
)

module.exports = router