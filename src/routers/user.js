// Require downloaded modules
const express = require('express')
const multer = require('multer')
const sharp = require('sharp')

// Middleware function for authentification verification
const auth = require('../middleware/auth')

// User schema
const User = require('../models/user')

const router = new express.Router()

// Function to upload an image to the database
const upload = multer(
    {
        limits: {
            fileSize: 1000000
        },
        fileFilter(req, file, cb)
        {
            if (!file.originalname.match(/\.(jpg|jpeg|png)/))
            {
                return cb(
                    new Error('Please upload a PDF')
                )
            }

            cb(undefined, true)
        }
    }
)

// Create a new user
router.post(
    '/users',
    async (req, res) => 
    {
        const user = new User(req.body)

        try
        {
            const token = await user.generateAuthToken()
            await user.save()

            res.status(201).send({ user, token })
        }
        catch (error)
        {
            res.status(400).send(error)
        }
    }
)


// Login an user
router.post(
    '/users/login',
    async (req, res) =>
    {
        try {
            const user = await User.findByCredentials(
                req.body.email,
                req.body.password
            )
            const token = await user.generateAuthToken()

            res.send({ user, token })

        } catch (error) {
            res.status(400).send(error)
        }
    }
)


// User Logout 
router.post(
    '/users/logout',
    auth,
    async (req, res) =>
    {
        try 
        {
            req.user.tokens = req.user.tokens.filter(
                token => token.token !== req.token
            )
            await req.user.save()

            res.send()
        } 
        catch (error) 
        {
            res.status(500).send(error)
        }
    }
)


// Logout with all authentification token
router.post(
    '/users/logoutAll',
    auth,
    async (req, res) =>
    {
        try 
        {
            req.user.tokens = []
            await req.user.save()

            res.send()
        } 
        catch (error) 
        {
            res.status(500).send(error)
        }
    }
)


// User upload an avatar
router.post(
    '/users/me/avatar',
    auth,
    upload.single('avatar'),
    async (req, res) => 
    {
        const buffer = await sharp(req.file.buffer)
        .resize({ width: 250, height: 250 }).png().toBuffer()

        req.user.avatar = buffer

        await req.user.save()
        res.send()
    },
    (error, req, res, next) =>
    {
        res.status(400).send({ error: error.message })
    }
)

// give user personal profile
router.get(
    '/users/me',
    auth,
    async (req, res) => res.send(req.user)
)


router.get(
    '/users/:id/avatar',
    async (req, res) =>
    {
        try 
        {
            const user = await User.findById(req.params.id)

            if (!user || !user.avatar)
            {
                throw new Error()
            }

            res.set('Content-Type', 'image/png')
            res.send(user.avatar)
        } 
        catch (error) 
        {
            res.status(404).send()
        }
    }
)

router.patch(
    '/users/me',
    auth,
    async (req, res) =>
    {
        const allowedUpdates = ['name', 'email', 'password', 'age']
        const updates = Object.keys(req.body)

        const isValidOperation = updates.every(
            update => allowedUpdates.includes(update)
        )

        if (!isValidOperation) {
            return res.status(400).send({ error: 'Invalid updates!' })
        }

        try 
        {
            updates.forEach(update => req.user[update] = req.body[update])
            await req.user.save()

            res.send(req.user)
        } 
        catch (error) 
        {
            res.status(500).send(error)
        }
    }
)

router.delete(
    '/users/me',
    auth,
    async (req, res) =>
    {
        try 
        {
            await req.user.remove()
            res.status(200).send(req.user)
        } 
        catch (error) 
        {
            res.status(500).send()
        }
    }
)

router.delete(
    '/users/me/avatar',
    auth,
    async (req, res) => 
    {
        req.user.avatar = undefined
        await req.user.save()
        res.send()
    }
)

module.exports = router