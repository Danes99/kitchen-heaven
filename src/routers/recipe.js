// Require downloaded modules
const express = require('express')

// Require MongoDB models
const Recipe = require('../models/recipe')

// Require authentification middleware
const auth = require('../middleware/auth')

const router = new express.Router()

// Return HTML form to create a new recipe
router.get(
    '/recipeForm',
    (req, res) =>
    {
        res.render(
            'post_recipe',
            {
                title: 'Post recipe'
            }
        ) 
    }
)

// Take the arguments and post a new recipe in the DataBase
router.post(
    '/recipes',
    async (req, res) => 
    {
        console.log(req.body)

        try
        {
            const recipe = new Recipe(
                {
                    name: req.body.recipeName,
                    country: req.body.country,
                    language: req.body.language,
                    description: req.body.description,
                    owner: req.body._id
                }
            )

            await recipe.save()
            res.status(201).send(recipe)
        }
        catch (error)
        {
            res.status(400).send(error)
        }
    }
)

// Return all of the user's recipe
// Need to be authenticated
router.get(
    '/recipes',
    auth,
    async (req, res) =>
    {
        const match = {}
        const sort = {}

        if (req.query.completed)
        {
            match.completed = req.query.completed === 'true'
        }

        if (req.query.sortBy)
        {
            const parts = req.query.sortBy.split(':')
            sort[parts[0]] = parts[1] === 'desc' ? -1 : 1
        }

        try
        {
            await req.user.populate(
                {
                    path: 'recipes',
                    match,
                    options: {
                        limit: parseInt(req.query.limit),
                        skip: parseInt(req.query.skip),
                        sort
                    }
                }
            )
            .execPopulate()

            res.send(req.user.recipes)
        }
        catch (error)
        {
            res.status(500).send(error)
        }
    }
)

// Get a recipe by id
router.get(
    '/recipes/:id',
    async (req, res) =>
    {
        // const _id = req.params.id
        
        try
        {
            // recipe = await Recipe.findById(_id)
            const recipe = await Recipe.findById(req.params.id)

            if (!recipe)
            {
                return res.status(404).send()
            }

            // res.status(201).send(recipe)
             
            res.render(
                'elhadji/page_recette.hbs',
                {
                    title: 'Recipe',
                    recipeName: recipe.name,
                    country: recipe.country,
                    language: recipe.language,
                    description: recipe.description
                }
            )
        }
        catch (error)
        {
            res.status(500).send()
        }
    }
)

// Modify a recipe by id
// Need to be authenticated
router.patch(
    '/recipes/:id',
    auth,
    async (req, res) =>
    {
        const updates = Object.keys(req.body)
        const allowedUpdates = ['completed', 'description']
        
        const isValidOperation = updates.every(
            update => allowedUpdates.includes(update)
        )
        
        if (!isValidOperation) 
        {
            return res.status(400).send({ error: 'Invalid updates!' })
        }

        try 
        {
            // const recipe = await Recipe.findById(req.params.id)

            const recipe = await Recipe.findOne(
                {
                    _id: req.params.id,
                    owner: req.user._id
                }
            )

            if (!recipe) {
                return res.status(404).send()
            }

            updates.forEach(update => recipe[update] = req.body[update])
            await recipe.save()

            res.send(recipe)
        } 
        catch (error) 
        {
            res.status(500).send(error)
        }
    }
)

// Delete a recipe by id
// Need to be authenticated
router.delete(
    '/recipes/:id',
    auth,
    async (req, res) =>
    {
        try 
        {
            const recipe = await Recipe.findOneAndDelete(
                {
                    _id: req.params.id,
                    owner: req.user._id
                }
            )

            if(!recipe) {
                return res.status(404).send()
            }

            res.status(201).send(recipe)
        } 
        catch (error) 
        {
            res.status(500).send()
        }
    }
)

module.exports = router