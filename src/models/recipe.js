const mongoose = require('mongoose')

const recipeSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
            trim: true
        },
        country: {
            type: String,
            required: true,
            trim: true
        },
        language: {
            type: String,
            required: true,
            trim: true
        },
        description: { 
            type: String,
            required: true,
            trim: true
        },
        owner: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'User'
        }
    },
    {
        timestamps: true
    }
)

const Recipe = mongoose.model(
    'Recipe',
    recipeSchema
)

module.exports = Recipe