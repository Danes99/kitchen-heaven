# Kitchen Heaven

Kitchen Heaven is a web application that allows you to search a recipe by recipe name, country, language & more.

## Description

Module List:
* bcryptjs@2.4.3
* express@4.17.1
* hbs@4.1.1
* jsonwebtoken@8.5.1
* mongoose@5.9.13
* multer@1.4.2
* sharp@0.25.2
* validator@13.0.0

Developpment modules:
* env-cmd@10.1.0
* nodemon@2.0.3

## Installation

Use the node package manager [npm](https://www.npmjs.com/) to install the libraries.

```bash
npm install
```

Need to install [MongoDB](https://www.mongodb.com/).
User interface for MongoDB [Robo 3T](https://robomongo.org/).

## Usage

Normal use
```bash
npm run start
```

Developer mode
```bash
npm run dev
```

Start the database: the "..." means the path where you have intalled MongoDB.
```bash
.../mongodb/bin/mongod --dbpath=.../mongodb-data
```

For me the command is
```bash
/Users/username/mongodb/bin/mongod --dbpath=/Users/username/mongodb-data
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)